#include "igt.h"
#include "igt_kmod.h"

IGT_TEST_DESCRIPTION("XE driver live kunit tests");

/**
 * TEST: Run Xe Kunit live tests
 * Category: Software building block
 * Test category: functionality test
 * Run type: live KUnit
 * Test requirements: CONFIG_DRM_XE_KUNIT_TEST should be enabled
 *
 * SUBTEST: %s
 * Sub-category: %arg[1]
 * Description: run %arg[1] life selftest
 *
 * arg[1]:
 *
 * @dma-buf:	dma-buf
 * @bo:		bo
 * @migrate:	migrate
 */

igt_main
{
	igt_subtest("dma-buf")
		igt_kunit("xe_dma_buf_test", NULL);

	igt_subtest("bo")
		igt_kunit("xe_bo_test", NULL);

	igt_subtest("migrate")
		igt_kunit("xe_migrate_test", NULL);
}

