/*
 * Copyright © 2022 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * Authors:
 *    Matthew Brost <matthew.brost@intel.com>
 */

#include <stdlib.h>
#include <pthread.h>

#include "drmtest.h"
#include "ioctl_wrappers.h"
#include "igt_map.h"

#include "xe_query.h"
#include "xe_ioctl.h"

static struct drm_xe_query_config *xe_query_config_new(int fd)
{
	struct drm_xe_query_config *config;
	struct drm_xe_device_query query = {
		.extensions = 0,
		.query = DRM_XE_DEVICE_QUERY_CONFIG,
		.size = 0,
		.data = 0,
	};

	igt_assert_eq(igt_ioctl(fd, DRM_IOCTL_XE_DEVICE_QUERY, &query), 0);

	config = malloc(query.size);
	igt_assert(config);

	query.data = to_user_pointer(config);
	igt_assert_eq(igt_ioctl(fd, DRM_IOCTL_XE_DEVICE_QUERY, &query), 0);

	igt_assert(config->num_params > 0);

	return config;
}

static struct drm_xe_query_gts *xe_query_gts_new(int fd)
{
	struct drm_xe_query_gts *gts;
	struct drm_xe_device_query query = {
		.extensions = 0,
		.query = DRM_XE_DEVICE_QUERY_GTS,
		.size = 0,
		.data = 0,
	};

	igt_assert_eq(igt_ioctl(fd, DRM_IOCTL_XE_DEVICE_QUERY, &query), 0);

	gts = malloc(query.size);
	igt_assert(gts);

	query.data = to_user_pointer(gts);
	igt_assert_eq(igt_ioctl(fd, DRM_IOCTL_XE_DEVICE_QUERY, &query), 0);

	return gts;
}

static uint64_t __memory_regions(const struct drm_xe_query_gts *gts)
{
	uint64_t regions = 0;
	int i;

	for (i = 0; i < gts->num_gt; i++)
		regions |= gts->gts[i].native_mem_regions |
			   gts->gts[i].slow_mem_regions;

	return regions;
}

static struct drm_xe_engine_class_instance *
xe_query_engines_new(int fd, int *num_engines)
{
	struct drm_xe_engine_class_instance *hw_engines;
	struct drm_xe_device_query query = {
		.extensions = 0,
		.query = DRM_XE_DEVICE_QUERY_ENGINES,
		.size = 0,
		.data = 0,
	};

	igt_assert(num_engines);
	igt_assert_eq(igt_ioctl(fd, DRM_IOCTL_XE_DEVICE_QUERY, &query), 0);

	hw_engines = malloc(query.size);
	igt_assert(hw_engines);

	query.data = to_user_pointer(hw_engines);
	igt_assert_eq(igt_ioctl(fd, DRM_IOCTL_XE_DEVICE_QUERY, &query), 0);

	*num_engines = query.size / sizeof(*hw_engines);

	return hw_engines;
}

static struct drm_xe_query_mem_usage *xe_query_mem_usage_new(int fd)
{
	struct drm_xe_query_mem_usage *mem_usage;
	struct drm_xe_device_query query = {
		.extensions = 0,
		.query = DRM_XE_DEVICE_QUERY_MEM_USAGE,
		.size = 0,
		.data = 0,
	};

	igt_assert_eq(igt_ioctl(fd, DRM_IOCTL_XE_DEVICE_QUERY, &query), 0);

	mem_usage = malloc(query.size);
	igt_assert(mem_usage);

	query.data = to_user_pointer(mem_usage);
	igt_assert_eq(igt_ioctl(fd, DRM_IOCTL_XE_DEVICE_QUERY, &query), 0);

	return mem_usage;
}

/* FIXME: Make generic / multi-GT aware */
static uint64_t __mem_vram_size(struct drm_xe_query_mem_usage *mem_usage)
{
	for (int i = 0; i < mem_usage->num_regions; i++)
		if (mem_usage->regions[i].mem_class == XE_MEM_REGION_CLASS_VRAM)
			return mem_usage->regions[i].total_size;

	return 0;
}

static bool __mem_has_vram(struct drm_xe_query_mem_usage *mem_usage)
{
	for (int i = 0; i < mem_usage->num_regions; i++)
		if (mem_usage->regions[i].mem_class == XE_MEM_REGION_CLASS_VRAM)
			return true;

	return false;
}

static uint32_t __mem_default_alignment(struct drm_xe_query_mem_usage *mem_usage)
{
	uint32_t alignment = XE_DEFAULT_ALIGNMENT;

	for (int i = 0; i < mem_usage->num_regions; i++)
		if (alignment < mem_usage->regions[i].min_page_size)
			alignment = mem_usage->regions[i].min_page_size;

	return alignment;
}

static bool xe_check_supports_faults(int fd)
{
	bool supports_faults;

	struct drm_xe_vm_create create = {
		.flags = DRM_XE_VM_CREATE_ASYNC_BIND_OPS |
			 DRM_XE_VM_CREATE_FAULT_MODE,
	};

	supports_faults = !igt_ioctl(fd, DRM_IOCTL_XE_VM_CREATE, &create);

	if (supports_faults)
		xe_vm_destroy(fd, create.vm_id);

	return supports_faults;
}

const char* xe_engine_class_string(uint32_t engine_class)
{
	switch (engine_class) {
		case DRM_XE_ENGINE_CLASS_RENDER:
			return "DRM_XE_ENGINE_CLASS_RENDER";
		case DRM_XE_ENGINE_CLASS_COPY:
			return "DRM_XE_ENGINE_CLASS_COPY";
		case DRM_XE_ENGINE_CLASS_VIDEO_DECODE:
			return "DRM_XE_ENGINE_CLASS_VIDEO_DECODE";
		case DRM_XE_ENGINE_CLASS_VIDEO_ENHANCE:
			return "DRM_XE_ENGINE_CLASS_VIDEO_ENHANCE";
		case DRM_XE_ENGINE_CLASS_COMPUTE:
			return "DRM_XE_ENGINE_CLASS_COMPUTE";
		default: return "?";
	}
}

static struct xe_device_cache {
	pthread_mutex_t cache_mutex;
	struct igt_map *map;
} cache;

static struct xe_device *find_in_cache_unlocked(int fd)
{
	return igt_map_search(cache.map, from_user_pointer(fd));
}

static struct xe_device *find_in_cache(int fd)
{
	struct xe_device *xe_dev;

	pthread_mutex_lock(&cache.cache_mutex);
	xe_dev = find_in_cache_unlocked(fd);
	pthread_mutex_unlock(&cache.cache_mutex);

	return xe_dev;
}

struct xe_device *xe_device_get(int fd)
{
	struct xe_device *xe_dev;

	xe_dev = find_in_cache(fd);
	if (xe_dev) {
		igt_debug("Find in cache, fd: %d\n", fd);
		return xe_dev;
	}
	igt_debug("Not found in the cache, allocating\n");

	xe_dev = calloc(1, sizeof(*xe_dev));
	igt_assert(xe_dev);

	xe_dev->fd = fd;
	xe_dev->config = xe_query_config_new(fd);
	xe_dev->number_gt = xe_dev->config->info[XE_QUERY_CONFIG_GT_COUNT];
	xe_dev->va_bits = xe_dev->config->info[XE_QUERY_CONFIG_VA_BITS];
	xe_dev->rev_dev_id = xe_dev->config->info[XE_QUERY_CONFIG_REV_AND_DEVICE_ID];
	xe_dev->gts = xe_query_gts_new(fd);
	xe_dev->memory_regions = __memory_regions(xe_dev->gts);
	xe_dev->hw_engines = xe_query_engines_new(fd, &xe_dev->number_hw_engines);
	xe_dev->mem_usage = xe_query_mem_usage_new(fd);
	xe_dev->vram_size = __mem_vram_size(xe_dev->mem_usage);
	xe_dev->default_alignment = __mem_default_alignment(xe_dev->mem_usage);
	xe_dev->has_vram = __mem_has_vram(xe_dev->mem_usage);
	xe_dev->supports_faults = xe_check_supports_faults(fd);

	igt_map_insert(cache.map, from_user_pointer(fd), xe_dev);

	return xe_dev;
}

static void xe_device_free(struct xe_device *xe_dev)
{
	igt_debug("free device: %d\n", xe_dev->fd);
	free(xe_dev->config);
	free(xe_dev->gts);
	free(xe_dev->hw_engines);
	free(xe_dev->mem_usage);
	free(xe_dev);
}

static void delete_in_cache(struct igt_map_entry *entry)
{
	xe_device_free((struct xe_device *)entry->data);
}

void xe_device_put(int fd)
{
	pthread_mutex_lock(&cache.cache_mutex);
	if (find_in_cache_unlocked(fd))
		igt_map_remove(cache.map, from_user_pointer(fd), delete_in_cache);
	pthread_mutex_unlock(&cache.cache_mutex);
}

static void xe_device_destroy_cache(void)
{
	pthread_mutex_lock(&cache.cache_mutex);
	igt_map_destroy(cache.map, delete_in_cache);
	pthread_mutex_unlock(&cache.cache_mutex);
}

static void xe_device_cache_init(void)
{
	pthread_mutex_init(&cache.cache_mutex, NULL);
	xe_device_destroy_cache();
	cache.map = igt_map_create(igt_map_hash_32, igt_map_equal_32);
}

#define RETV(__v) \
	struct xe_device *xe_dev;\
	xe_dev = find_in_cache(fd);\
	igt_assert(xe_dev);\
	return xe_dev->__v

int xe_number_gt(int fd)
{
	RETV(number_gt);
}

uint64_t all_memory_regions(int fd)
{
	RETV(memory_regions);
}

uint64_t system_memory(int fd)
{
	uint64_t regions = all_memory_regions(fd);

	return regions & 0x1;
}

uint64_t vram_memory(int fd, int gt)
{
	uint64_t regions = all_memory_regions(fd);

	return regions & (0x2 << gt);
}

uint64_t vram_if_possible(int fd, int gt)
{
	uint64_t regions = all_memory_regions(fd);
	uint64_t system_memory = regions & 0x1;
	uint64_t vram = regions & (0x2 << gt);

	return vram ? vram : system_memory;
}

struct drm_xe_engine_class_instance *xe_hw_engines(int fd)
{
	RETV(hw_engines);
}

struct drm_xe_engine_class_instance *xe_hw_engine(int fd, int idx)
{
	struct drm_xe_engine_class_instance *engines = xe_hw_engines(fd);

	return &engines[idx];
}

int xe_number_hw_engines(int fd)
{
	RETV(number_hw_engines);
}

bool xe_has_vram(int fd)
{
	RETV(has_vram);
}

uint64_t xe_vram_size(int fd)
{
	RETV(vram_size);
}

uint32_t xe_get_default_alignment(int fd)
{
	RETV(default_alignment);
}

bool xe_supports_faults(int fd)
{
	RETV(supports_faults);
}

uint32_t xe_va_bits(int fd)
{
	RETV(va_bits);
}

uint32_t xe_dev_id(int fd)
{
	RETV(rev_dev_id) & 0xffff;
}

igt_constructor
{
	xe_device_cache_init();
}
