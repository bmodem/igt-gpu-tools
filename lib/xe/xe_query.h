/*
 * Copyright © 2022 Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 * Authors:
 *    Matthew Brost <matthew.brost@intel.com>
 */

#ifndef XE_QUERY_H
#define XE_QUERY_H

#include <stdint.h>
#include <xe_drm.h>
#include "igt_list.h"

#define XE_DEFAULT_ALIGNMENT           0x1000
#define XE_DEFAULT_ALIGNMENT_64K       0x10000

struct xe_device {
	int fd;

	struct drm_xe_query_config *config;
	struct drm_xe_query_gts *gts;
	uint64_t memory_regions;
	struct drm_xe_engine_class_instance *hw_engines;
	int number_hw_engines;
	struct drm_xe_query_mem_usage *mem_usage;
	uint64_t vram_size;
	uint32_t default_alignment;
	bool has_vram;
	bool supports_faults;

	int number_gt;
	uint32_t va_bits;
	uint32_t rev_dev_id;
};

#define for_each_hw_engine(__fd, __hwe) \
	for (int __i = 0; __i < xe_number_hw_engines(__fd) && \
	     (__hwe = xe_hw_engine(__fd, __i)); ++__i)
#define for_each_hw_engine_class(__class) \
	for (__class = 0; __class < DRM_XE_ENGINE_CLASS_COMPUTE + 1; \
	     ++__class)
#define for_each_gt(__fd, __gt) \
	for (__gt = 0; __gt < xe_number_gt(__fd); ++__gt)

int xe_number_gt(int fd);
uint64_t all_memory_regions(int fd);
uint64_t system_memory(int fd);
uint64_t vram_memory(int fd, int gt);
uint64_t vram_if_possible(int fd, int gt);
struct drm_xe_engine_class_instance *xe_hw_engines(int fd);
struct drm_xe_engine_class_instance *xe_hw_engine(int fd, int idx);
int xe_number_hw_engines(int fd);
bool xe_has_vram(int fd);
uint64_t xe_vram_size(int fd);
uint32_t xe_get_default_alignment(int fd);
uint32_t xe_va_bits(int fd);
uint32_t xe_dev_id(int fd);
bool xe_supports_faults(int fd);
const char* xe_engine_class_string(uint32_t engine_class);

struct xe_device *xe_device_get(int fd);
void xe_device_put(int fd);

#endif	/* XE_QUERY_H */
