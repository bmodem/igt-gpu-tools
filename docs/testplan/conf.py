# -*- coding: utf-8 -*-

import sys
import os

extensions = []

source_suffix = '.rst'
master_doc = 'index'

project = 'IGT Test Tools'
copyright = 'Intel'
author = 'IGT developers'
language = 'en'

exclude_patterns = []
todo_include_todos = False

html_theme = "nature"

html_css_files = []
html_static_path = ['.']
html_copy_source = False

html_use_smartypants = False
html_sidebars = { '**': ['searchbox.html', 'localtoc.html']}
htmlhelp_basename = 'IGT'

html_theme_options = {
    "body_max_width": "1520px",
    "sidebarwidth": "400px",
}
